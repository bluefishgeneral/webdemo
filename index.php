<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Idea Pour - Predict Your Thoughts</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
<script>
<?php
echo file_get_contents('js/jquery.loadingModal.js');
?>
</script>
<style>
<?php
 echo file_get_contents('css/jquery.loadingModal.css');
?>
</style>
<meta name="verifyownership" 
 content="c71f5ee2b440a1424d6a51beba3d62d7"/>
  </head>
  <body >
  <div  id="video"   style="background-color:gray;">
    <center>
    <iframe   style="height:35vh;width:100%;" src="https://www.youtube.com/embed/SYp-PqpM-ss" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </center>
   </div>
   <div id="text_in">
    <button id="speak" style="width:100%;">Play With Sound 🔊</button>
    <button id="stop" style="width:100%;">Stop Voice ⏹</button>
    <div id="summernote"  style="background-color:white;height=100%">This application predicts what you are going to write next. So you need to start with something. This is fine too.</div>
     
    <button style="width:100%;position: fixed;
    bottom: 0;
    right: 0;" id="predict" type="button" class="btn btn-primary">PREDICT</button><!--
    <iframe src='https://deadsimplechat.com/luq6FcHuJ' width='100%' height='500px'></iframe>-->
    </div>
   
  </body>
  <script>
    function ViewPortHeight()
{
var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
//var viewsize = w + "," + h;
return h;
}
function showModal(ELEMENT) {
        ELEMENT.loadingModal({text: 'Showing loader animations...'});

        var delay = function(ms){ return new Promise(function(r) { setTimeout(r, ms) }) };
        var time = 2000;

        delay(time)
                .then(function() { $('body').loadingModal('animation', 'rotatingPlane').loadingModal('backgroundColor', 'red'); return delay(time);})
                .then(function() { $('body').loadingModal('animation', 'wave'); return delay(time);})
                .then(function() { $('body').loadingModal('animation', 'wanderingCubes').loadingModal('backgroundColor', 'green'); return delay(time);})
                .then(function() { $('body').loadingModal('animation', 'spinner'); return delay(time);})
                .then(function() { $('body').loadingModal('animation', 'chasingDots').loadingModal('backgroundColor', 'blue'); return delay(time);})
                .then(function() { $('body').loadingModal('animation', 'threeBounce'); return delay(time);})
                .then(function() { $('body').loadingModal('animation', 'circle').loadingModal('backgroundColor', 'black'); return delay(time);})
                .then(function() { $('body').loadingModal('animation', 'cubeGrid'); return delay(time);})
                .then(function() { $('body').loadingModal('animation', 'fadingCircle').loadingModal('backgroundColor', 'gray'); return delay(time);})
                .then(function() { $('body').loadingModal('animation', 'foldingCube'); return delay(time); } )
                .then(function() { $('body').loadingModal('color', 'black').loadingModal('text', 'Done :-)').loadingModal('backgroundColor', 'yellow');  return delay(time); } )
                .then(function() { $('body').loadingModal('hide'); return delay(time); } )
                .then(function() { $('body').loadingModal('destroy') ;} );
    }
      $('#speak').click(function() {
          if ('speechSynthesis' in window) {
                 var msg = new SpeechSynthesisUtterance();
                msg.text = $($(".note-editable")[0]).text().trim();
                window.speechSynthesis.speak(msg);
                
            }else{
              alert("Sorry, your browser doesn't support text to speech!");
            }

      });
      $('#stop').click(function() {
          if ('speechSynthesis' in window) {
                 window.speechSynthesis.cancel();
                
            }else{
              alert("Sorry, your browser doesn't support text to speech!");
            }

      });
      
      
      

      $('#summernote').summernote({
        placeholder: 'Type here and click "PREDICT" to predict the next bunch of words...',
        tabsize: 2,
        height:ViewPortHeight()*0.5,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'picture', 'video']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ]
      });
      //.replace(/[//s]/g, '+').replace(/[^a-zA-Z0-9-_\+]/g, '')
      $( "#predict" ).click(function() {
          var URL = "app_web_hook.php/?url_file=book_app_url&txt_in="+encodeURIComponent($($(".note-editable")[0]).text().trim())+"&ver=02&len=200";
          console.log("start");
 	   $('#text_in').loadingModal({
  position: 'auto',
  text: '',
  color: '#fff',
  opacity: '0.7',
  backgroundColor: 'rgb(255,0,0)',
  animation: 'wave'
});
           setTimeout(function(){
    $.ajax({
		url: URL,
		type: 'GET',
		headers: {'Access-Control-Allow-Origin': '*'},
		dataType: 'html', // added data type
		success: function(res) {
		    console.log(res + "| > |" + $($(".note-editable")[0]).text()+"| |"+URL);
		    //alert(res);
		    $($(".note-editable")[0]).text($($(".note-editable")[0]).text().substring(0, $($(".note-editable")[0]).text().length-Math.min(200, $($(".note-editable")[0]).text().length))+res);
		   
	 		$('#text_in').loadingModal('destroy');

		}
           });
}, 0);
     });
    </script>
</html>


